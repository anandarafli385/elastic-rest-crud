package com.elastic.demo.service;

import com.elastic.demo.document.Pegawai;
import com.elastic.demo.repository.PegawaiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
//@Component
public class PegawaiService {

    private final PegawaiRepository repo;

    @Autowired
    public PegawaiService(PegawaiRepository repo){
        this.repo = repo;
    }

    public void save(Pegawai pegawai){
        repo.save(pegawai);
    }

    public void delete(String id){
        repo.deleteById(id);
    }

    public Pegawai findById(String id){
        return repo.findById(id).orElse(null);
    }
}
