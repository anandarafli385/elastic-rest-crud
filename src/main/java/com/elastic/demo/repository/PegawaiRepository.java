package com.elastic.demo.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import com.elastic.demo.document.Pegawai;
import org.springframework.stereotype.Repository;

@Repository
public interface PegawaiRepository extends ElasticsearchRepository<Pegawai, String> {

//    void updatePegawai(String Id);
}
