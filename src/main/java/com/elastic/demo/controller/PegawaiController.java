package com.elastic.demo.controller;

import com.elastic.demo.document.Pegawai;
import com.elastic.demo.service.PegawaiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "api/pegawai")
public class PegawaiController {
    private final PegawaiService psrvc;

    @Autowired
    public PegawaiController(PegawaiService psrvc){
        this.psrvc = psrvc;
    }

    @PostMapping
    public void save(@RequestBody final Pegawai pegawai){
        psrvc.save(pegawai);
    }

    @GetMapping("/{id}")
    public Pegawai findById(@PathVariable final String id){
        return psrvc.findById(id);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteById(@PathVariable("id") final String id){
        psrvc.delete(id);
    }
}
